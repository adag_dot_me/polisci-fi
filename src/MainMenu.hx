import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Draw;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Text;
import menu.MenuText;
import menu.MenuPortrait;
import menu.RotatingBackground;
#if flash
import flash.display.BitmapData;
#end
#if mac
import native.display.BitmapData;
#end

class MainMenu extends Scene
{
	private var button:Image;
	private var button2:Image;
	private var newGame:MenuText;
	private var existingGame:MenuText;
	private var title:MenuText;
	private var message:String;
	private var msgText:MenuText;
	private var bg:RotatingBackground;
	private var prt:MenuPortrait;

	public function new(?msg:String)
	{
		if (msg != null) {
			message = msg;
		}
		super();
	}

	public override function begin()
	{
		title = new MenuText("PoliSci-Fi", 290, 40, 40, "font/AmpedExpItalic.otf");
		add(title);

		bg = new RotatingBackground(600, 400);
		add(bg);

		prt = new MenuPortrait(0, 358, 0);
		add(prt);

		if (message != null) {
			msgText = new MenuText(message, 400, 150, null, null, true);
			add(msgText);
		}

		newGame = new MenuText("New Game", 347, 210, 20);
		add(newGame);
		//newGame = new Text("New Game");
		//newGame.x = 350;
		//newGame.y = 225;
		//newGame.size = 20;

		existingGame = new MenuText("Existing Game", 330, 310, 20);
		add(existingGame);

		button = new Image("gfx/button.png");
		button.color = 0x393939;
		button.x = 300;
		button.y = 200;
		addGraphic(button);

		button2 = new Image("gfx/button.png");
		button2.color = 0x393939;
		button2.x = 300;
		button2.y = 300;
		addGraphic(button2);
	}

	public override function update()
	{
		//button.createRect(100, 50, button.color);
		//picture = new BitmapData(800, 600);
		//Draw.setTarget(picture);
		//Draw.text("New Game", 325, 210, { size : 30, color : 0xFFFFFF });
		checkMouse();
		//checkClicked();
		super.update();
	}

	private function checkMouse()
	{
		if (Input.mouseX > 300 && Input.mouseX < 500 && Input.mouseY > 200 && Input.mouseY < 250) {
			button.color = 0x525252;
			button2.color = 0x393939;
			if (Input.mousePressed) {
				remove(newGame);
				remove(existingGame);
				remove(title);

				if (message != null) {
					remove(msgText);
				}

				HXP.scene = new NewGame();
			}
		}
		else if (Input.mouseX > 300 && Input.mouseX < 500 && Input.mouseY > 300 && Input.mouseY < 350) {
			button.color = 0x393939;
			button2.color = 0x525252;
			if (Input.mousePressed) {
				remove(newGame);
				remove(existingGame);
				remove(title);

				if (message != null) {
					remove(msgText);
				}

				HXP.scene = new ExistingGame();
			}
		}
		else {
			button.color = 0x393939;
			button2.color = 0x393939;
		}
	}
}