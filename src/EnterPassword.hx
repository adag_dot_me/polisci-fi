import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import menu.MenuText;
import menu.TextInput;
import menu.RotatingBackground;
import menu.MenuPortrait;
import sys.io.File;
import sys.FileSystem;
import game.GameJSON;
import game.PercentJSON;
import nme.filesystem.File;
import tjson.TJSON;

class EnterPassword extends Scene {
	//var sav = new GameJSON(1, 1, passInput.text, Std.random(3), 100000, "none", Std.random(3), 100000, plan1rand, plan1rand, 0, 0, Math.random(), Math.random(), Math.random(), plan2rand, plan2rand, 0, 0, Math.random(), Math.random(), Math.random(), plan3rand, plan3rand, 0, 0, Math.random(), Math.random(), Math.random(), plan4rand, plan4rand, 0, 0, Math.random(), Math.random(), Math.random(), plan5rand, plan5rand, 0, 0, Math.random(), Math.random(), Math.random(), plan6rand, plan6rand, 0, 0, Math.random(), Math.random(), Math.random());
	private var gameData:Dynamic;
	private var button:Image;
	private var box2:Image;
	private var name:MenuText;
	private var pass:MenuText;
	private var passInput:TextInput;
	private var savePass:MenuText;
	private var title:MenuText;
	//private var sav:String;
	private var bg:RotatingBackground;
	private var prt:MenuPortrait;
	private var fnam:String;

	public function new(s:String)
	{
		fnam = s;
		super();
	}

	public override function begin()
	{
		var fname = fnam + ".sav";
		if (sys.FileSystem.exists(fname) == true) {
			var fin = cpp.io.File.read(fname, false);
			var fintext = fin.readLine();
			gameData = TJSON.parse(fintext);
		}

		title = new MenuText("PoliSci-Fi", 290, 40, 40, "font/AmpedExpItalic.otf");
		add(title);

		bg = new RotatingBackground(600, 400);
		add(bg);

		prt = new MenuPortrait(506, 335, 1);
		add(prt);

		pass = new MenuText("Enter password:", 100, 215, 20);
		add(pass);

		box2 = new Image("gfx/button.png");
		box2.color = 0x393939;
		box2.x = 300;
		box2.y = 200;
		addGraphic(box2);

		passInput = new TextInput(307, 215, box2);
		add(passInput);

		button = new Image("gfx/button.png");
		button.color = 0x393939;
		button.x = 300;
		button.y = 300;
		addGraphic(button);

		savePass = new MenuText("Save Password", 320, 310, 20);
		add(savePass);
	}

	public override function update()
	{
		if (Input.mouseX > 300 && Input.mouseX < 500 && Input.mouseY > 300 && Input.mouseY < 350) {
			button.color = 0x525252;
			if (Input.mousePressed && passInput.text != "")
			{
				remove(pass);
				remove(savePass);
				remove(title);

				var password = Std.string(passInput.text);
				remove(passInput);

				var fname = fnam + ".sav";
				var fname2 = fnam + ".dat";

				var fout = cpp.io.File.write(fname, false);
				var sav = new GameJSON(gameData.game.turn, gameData.game.active, gameData.game.playerone.password, gameData.game.playerone.portrait, gameData.game.playerone.money, password, gameData.game.playertwo.portrait, gameData.game.playertwo.money, gameData.game.planetone.population, gameData.game.planetone.neither, gameData.game.planetone.userone, gameData.game.planetone.usertwo, gameData.game.planetone.environ, gameData.game.planetone.econ, gameData.game.planetone.futurist, gameData.game.planettwo.population, gameData.game.planettwo.neither, gameData.game.planettwo.userone, gameData.game.planettwo.usertwo, gameData.game.planettwo.environ, gameData.game.planettwo.econ, gameData.game.planettwo.futurist, gameData.game.planetthree.population, gameData.game.planetthree.neither, gameData.game.planetthree.userone, gameData.game.planetthree.usertwo, gameData.game.planetthree.environ, gameData.game.planetthree.econ, gameData.game.planetthree.futurist, gameData.game.planetfour.population, gameData.game.planetfour.neither, gameData.game.planetfour.userone, gameData.game.planetfour.usertwo, gameData.game.planetfour.environ, gameData.game.planetfour.econ, gameData.game.planetfour.futurist, gameData.game.planetfive.population, gameData.game.planetfive.neither, gameData.game.planetfive.userone, gameData.game.planetfive.usertwo, gameData.game.planetfive.environ, gameData.game.planetfive.econ, gameData.game.planetfive.futurist, gameData.game.planetsix.population, gameData.game.planetsix.neither, gameData.game.planetsix.userone, gameData.game.planetsix.usertwo, gameData.game.planetsix.environ, gameData.game.planetsix.econ, gameData.game.planetsix.futurist);
				fout.writeString(sav.jsonstring);
				fout.close();

				fout = cpp.io.File.write(fname2, false);
				var dat = new PercentJSON(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
				fout.writeString(dat.jsonstring);
				fout.close();
				HXP.scene = new MainMenu("Password saved successfully.");
			}
		}
		else {
			button.color = 0x393939;
		}
		super.update();
	}
}