import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import menu.MenuText;
import menu.TextInput;
import menu.RotatingBackground;
import menu.MenuPortrait;
import sys.io.File;
import sys.FileSystem;
import game.GameJSON;
import game.PercentJSON;
import nme.filesystem.File;


class NewGame extends Scene
{
	private var button:Image;
	private var box:Image;
	private var box2:Image;
	private var name:MenuText;
	private var pass:MenuText;
	private var nameInput:TextInput;
	private var passInput:TextInput;
	private var createGame:MenuText;
	private var title:MenuText;
	private var sav:String;
	private var bg:RotatingBackground;
	private var prt:MenuPortrait;

	public function new()
	{
		super();
	}

	public override function begin()
	{
		title = new MenuText("PoliSci-Fi", 290, 40, 40, "font/AmpedExpItalic.otf");
		add(title);

		name = new MenuText("Enter game name:", 100, 215, 20);
		add(name);

		bg = new RotatingBackground(600, 400);
		add(bg);

		prt = new MenuPortrait(506, 335, 2);
		add(prt);

		box = new Image("gfx/button.png");
		box.color = 0x393939;
		box.x = 300;
		box.y = 200;
		addGraphic(box);

		nameInput = new TextInput(307, 215, box);
		add(nameInput);

		pass = new MenuText("Enter password:", 100, 315, 20);
		add(pass);

		box2 = new Image("gfx/button.png");
		box2.color = 0x393939;
		box2.x = 300;
		box2.y = 300;
		addGraphic(box2);

		passInput = new TextInput(307, 315, box2);
		add(passInput);

		button = new Image("gfx/button.png");
		button.color = 0x393939;
		button.x = 300;
		button.y = 400;
		addGraphic(button);

		createGame = new MenuText("Create Game", 320, 410, 20);
		add(createGame);
	}

	public override function update()
	{
		if (Input.mouseX > 300 && Input.mouseX < 500 && Input.mouseY > 400 && Input.mouseY < 450) {
			button.color = 0x525252;
			if (Input.mousePressed && nameInput.text.length > 1 && passInput.text.length > 1) {
				remove(name);
				remove(pass);
				remove(createGame);
				remove(title);

				trace(nameInput.text + " " + passInput.text);

				//var flpth = Sys.executablePath();
				//trace(flpth);

				var fname = Std.string(nameInput.text) + ".sav";
				var fname2 = Std.string(nameInput.text) + ".dat";

				//if (sys.FileSystem.exists(Std.string(nameInput.text) + ".txt") == false)
				if (sys.FileSystem.exists(fname) == false)
				{
					var fout = cpp.io.File.write(fname, false);

					var plan1rand = 1000000 + Std.random(9000000);
					var plan2rand = 1000000 + Std.random(9000000);
					var plan3rand = 1000000 + Std.random(9000000);
					var plan4rand = 1000000 + Std.random(9000000);
					var plan5rand = 1000000 + Std.random(9000000);
					var plan6rand = 1000000 + Std.random(9000000);

					var sav = new GameJSON(1, 1, passInput.text, Std.random(3), 100000, "none", Std.random(3), 100000, plan1rand, plan1rand, 0, 0, Math.random(), Math.random(), Math.random(), plan2rand, plan2rand, 0, 0, Math.random(), Math.random(), Math.random(), plan3rand, plan3rand, 0, 0, Math.random(), Math.random(), Math.random(), plan4rand, plan4rand, 0, 0, Math.random(), Math.random(), Math.random(), plan5rand, plan5rand, 0, 0, Math.random(), Math.random(), Math.random(), plan6rand, plan6rand, 0, 0, Math.random(), Math.random(), Math.random());

					//fout.writeString("{ '" + nameInput.text + "' : { 'playeronepass' :" + passInput.text + " } }");
					//sav = jsonCreate(1);
					fout.writeString(sav.jsonstring);
					fout.close();

					fout = cpp.io.File.write(fname2, false);

					var dat = new PercentJSON(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

					fout.writeString(dat.jsonstring);
					fout.close();

					remove(nameInput);
					remove(passInput);
					HXP.scene = new MainMenu();
				} else {
					trace("File " + fname + " already exists.");
					remove(nameInput);
					remove(passInput);
					HXP.scene = new MainMenu("File " + fname + " already exists.");
				}
			}
		} else {
			button.color = 0x393939;
		}

		//checkMouse();
		//checkClicked();
		super.update();
	}
}