import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Carousel;
import com.haxepunk.gui.Panel;
import menu.MenuText;
import menu.TextInput;
import menu.RotatingBackground;
import menu.MenuPortrait;
import sys.io.File;
import sys.FileSystem;
import game.GameJSON;
import game.PercentJSON;
import nme.filesystem.File;
import tjson.TJSON;
import nme.events.Event;

class GameScene extends Scene 
{
	private var prt:MenuPortrait;
	private var endTurn:MenuText;
	private var cashtext:MenuText;
	private var esttext:MenuText;
	private var environest:MenuText;
	private var econest:MenuText;
	private var futuristest:MenuText;
	private var swayText:MenuText;
	private var button:Image;
	private var textbg:Image;
	private var fnm:String;
	private var actv:Int;
	private var gameData:Dynamic;
	private var percentData:Dynamic;
	private var trn:MenuText;
	private var rtbg:RotatingBackground;
	private var car:Carousel;
	private var buttons:Array<Button>;
	private var research:Array<Int>;
	private var campaign:Array<Int>;
	private var environ:Array<Int>;
	private var econ:Array<Int>;
	private var futurist:Array<Int>;
	private var selected:Array<Int>;
	private var leftBt:Array<Button>;
	private var rightBt:Array<Button>;
	private var textBt:Array<Button>;
	private var panels:Array<Panel>;
	private var lbls:Array<Label>;
	private var envir:Array<Float>;
	private var econe:Array<Float>;
	private var fute:Array<Float>;
	private var envirplan:Array<Float>;
	private var econplan:Array<Float>;
	private var futplan:Array<Float>;
	private var population:Array<Int>;
	private var sway:Array<Int>;
	private var money:Float;
	private var totalCost:Float;
	private var totalResearch:Float;

	public function new(a:Int, f:String)
	{
		// UI initialization
		Control.defaultLayer = 100;

		Button.defaultPadding = 4;

		Label.defaultFont = nme.Assets.getFont("font/pf_ronda_seven.ttf");
		Label.defaultColor = 0x1E4E82;
		Label.defaultSize = 8;
		//End UI initialization

		selected = [];
		research = [];
		campaign = [];
		environ = [];
		econ = [];
		futurist = [];
		panels = [];
		leftBt = [];
		rightBt = [];
		textBt = [];
		lbls = [];
		envir = [];
		econe = [];
		fute = [];
		envirplan = [];
		econplan = [];
		futplan = [];
		population = [];
		sway = [];
		fnm = f;
		actv = a;
		super();
	}

	override public function begin()
	{
		var fname = fnm + ".sav";
		if (sys.FileSystem.exists(fname) == true) {
			var fin = cpp.io.File.read(fname, false);
			var fintext = fin.readLine();
			gameData = TJSON.parse(fintext);
		}

		fname = fnm + ".dat";
		if (sys.FileSystem.exists(fname) == true) {
			var fin = cpp.io.File.read(fname, false);
			var fintext = fin.readLine();
			percentData = TJSON.parse(fintext);
		}

		//Setting up necessary data
		if (actv == 1) {
			money = gameData.game.playerone.money / 1000;
		} else {
			money = gameData.game.playertwo.money / 1000;
		}

		envir[1] = percentData.percents.planetone.environ;
		envir[2] = percentData.percents.planettwo.environ;
		envir[3] = percentData.percents.planetthree.environ;
		envir[4] = percentData.percents.planetfour.environ;
		envir[5] = percentData.percents.planetfive.environ;
		envir[6] = percentData.percents.planetsix.environ;
		trace("envir");

		econe[1] = percentData.percents.planetone.econ;
		econe[2] = percentData.percents.planettwo.econ;
		econe[3] = percentData.percents.planetthree.econ;
		econe[4] = percentData.percents.planetfour.econ;
		econe[5] = percentData.percents.planetfive.econ;
		econe[6] = percentData.percents.planetsix.econ;
		trace("econe");

		fute[1] = percentData.percents.planetone.futurist;
		fute[2] = percentData.percents.planettwo.futurist;
		fute[3] = percentData.percents.planetthree.futurist;
		fute[4] = percentData.percents.planetfour.futurist;
		fute[5] = percentData.percents.planetfive.futurist;
		fute[6] = percentData.percents.planetsix.futurist;
		trace("fute");

		envirplan[1] = gameData.game.planetone.environ;
		envirplan[2] = gameData.game.planettwo.environ;
		envirplan[3] = gameData.game.planetthree.environ;
		envirplan[4] = gameData.game.planetfour.environ;
		envirplan[5] = gameData.game.planetfive.environ;
		envirplan[6] = gameData.game.planetsix.environ;
		trace("envirplan");

		econplan[1] = gameData.game.planetone.econ;
		econplan[2] = gameData.game.planettwo.econ;
		econplan[3] = gameData.game.planetthree.econ;
		econplan[4] = gameData.game.planetfour.econ;
		econplan[5] = gameData.game.planetfive.econ;
		econplan[6] = gameData.game.planetsix.econ;
		trace("econplan");

		futplan[1] = gameData.game.planetone.futurist;
		futplan[2] = gameData.game.planettwo.futurist;
		futplan[3] = gameData.game.planetthree.futurist;
		futplan[4] = gameData.game.planetfour.futurist;
		futplan[5] = gameData.game.planetfive.futurist;
		futplan[6] = gameData.game.planetsix.futurist;
		trace("futplan");

		population[1] = gameData.game.planetone.neither;
		population[2] = gameData.game.planettwo.neither;
		population[3] = gameData.game.planetthree.neither;
		population[4] = gameData.game.planetfour.neither;
		population[5] = gameData.game.planetfive.neither;
		population[6] = gameData.game.planetsix.neither;
		trace("population");

		if (actv == 1) {
			sway[1] = gameData.game.planetone.userone;
			sway[2] = gameData.game.planettwo.userone;
			sway[3] = gameData.game.planetthree.userone;
			sway[4] = gameData.game.planetfour.userone;
			sway[5] = gameData.game.planetfive.userone;
			sway[6] = gameData.game.planetsix.userone;
			trace("swayed");
		} else if (actv == 2) {
			sway[1] = gameData.game.planetone.usertwo;
			sway[2] = gameData.game.planettwo.usertwo;
			sway[3] = gameData.game.planetthree.usertwo;
			sway[4] = gameData.game.planetfour.usertwo;
			sway[5] = gameData.game.planetfive.usertwo;
			sway[6] = gameData.game.planetsix.usertwo;
			trace("swayed");
		}
		trace(gameData.game.planetone.userone);
		//End neccesary data setup

		if (actv != gameData.game.active) {
			HXP.scene = new MainMenu("It is not your turn.");
		}

		if (gameData.game.active == 1) {
			prt = new MenuPortrait(0, 358, gameData.game.playerone.portrait, 300);

			if (gameData.game.playerone.portrait == 0) {
				Control.useSkin("gfx/ui/blueMagda.png");
			}
			else if (gameData.game.playerone.portrait == 1) {
				Control.useSkin("gfx/ui/purpleMagda.png");
			}
			else if (gameData.game.playerone.portrait == 2) {
				Control.useSkin("gfx/ui/greenMagda.png");
			}
		}
		else if (gameData.game.active == 2) {
			prt = new MenuPortrait(0, 358, gameData.game.playertwo.portrait, 300);

			if (gameData.game.playertwo.portrait == 0) {
				Control.useSkin("gfx/ui/blueMagda.png");
			}
			else if (gameData.game.playertwo.portrait == 1) {
				Control.useSkin("gfx/ui/purpleMagda.png");
			}
			else if (gameData.game.playertwo.portrait == 2) {
				Control.useSkin("gfx/ui/greenMagda.png");
			}
		}

		add(prt);

		rtbg = new RotatingBackground(600, 400);
		add(rtbg);

		car = new Carousel(400, 200, 200, 100);

		var plyrpnl = new Panel(0, 0, 100, 20);
		var plyrlbl = new Label("Player" + actv);
		plyrlbl.localX = 30;
		plyrlbl.localY = 0;
		plyrpnl.addControl(plyrlbl);
		car.addControl(plyrpnl);
		/*var c:Control = new Control();
		c.graphic = new Image(Control.currentSkin);
		c.width = cast(c.graphic, Image).width;
		c.height = cast(c.graphic, Image).height;
		car.addControl(c);
		trace("control added");*/

		for (i in 1...7) {
			var pln1:Panel = new Panel(0, 0, 100, 70);
			var lbl = new Label("Planet " + i);
			lbl.localX = 30;
			lbl.localY = 0;
			pln1.addControl(lbl);
			trace("label " + i);

			var bt:Button = new Button("<");
			bt.localX = 5;
			bt.localY = 20;
			bt.addEventListener(Button.CLICKED, leftSelect);
			leftBt[i] = bt;
			pln1.addControl(bt);
			trace("bt1 " + i);

			var bt2:Button = new Button("Research");
			bt2.localX = 25;
			bt2.localY = 20;
			bt2.addEventListener(Button.CLICKED, centerSelect);
			textBt[i] = bt2;
			pln1.addControl(bt2);
			trace("bt2 " + i);

			var bt3:Button = new Button(">");
			bt3.localX = 80;
			bt3.localY = 20;
			bt3.addEventListener(Button.CLICKED, rightSelect);
			rightBt[i] = bt3;
			pln1.addControl(bt3);
			trace("bt3 " + i);

			var lbl2 = new Label("$0k");
			lbl2.localX = 30;
			lbl2.localY = 43;
			lbls[i] = lbl2;
			pln1.addControl(lbl2);

			panels[i] = pln1;
			selected[i] = 0;
			research[i] = 0;
			campaign[i] = 0;
			environ[i] = 0;
			econ[i] = 0;
			futurist[i] = 0;

			car.addControl(pln1);
			trace("pln1 added");
		}
		car.addEventListener(Carousel.SELECTION_CHANGED, printSelection);
		add(car);

		button = new Image("gfx/button.png");
		button.color = 0x393939;
		button.x = 550;
		button.y = 500;
		addGraphic(button);

		textbg = new Image("gfx/textbg.png");
		textbg.color = 0x393939;
		textbg.x = 50;
		textbg.y = 50;
		addGraphic(textbg);

		if (actv == 1) {
			cashtext = new MenuText("Money: $" + gameData.game.playerone.money, 75, 75, 20);
		} else if (actv == 2) {
			cashtext = new MenuText("Money: $" + gameData.game.playertwo.money, 75, 75, 20);
		}
		add(cashtext);

		esttext = new MenuText("Estimates:", 75, 105, 20);
		add(esttext);
		
		environest = new MenuText("Environ: " + Std.string(envir[car.selectedId]), 75, 135, 20);
		add(environest);

		econest = new MenuText("Econ: " + Std.string(econe[car.selectedId]), 75, 165, 20);
		add(econest);

		futuristest = new MenuText("Futurist: " + Std.string(fute[car.selectedId]), 75, 195, 20);
		add(futuristest);

		swayText = new MenuText("Swayed: " + Std.string(sway[car.selectedId]), 75, 225, 20);
		add(swayText);
		trace("swayText");

		endTurn = new MenuText("End Turn", 600, 515, 20);
		add(endTurn);

		trn = new MenuText("Turn " + gameData.game.turn, 600, 560, 20);
		add(trn);
	}

	override public function update()
	{
		arrayUpdate();
		//printSelection(1);

		if (Input.mouseX > 550 && Input.mouseX < 750 && Input.mouseY > 500 && Input.mouseY < 550) {
			button.color = 0x525252;
			if (Input.mousePressed) {
				if (actv == 1) {
					remove(endTurn);
					remove(trn);
					remove(cashtext);
					remove(esttext);
					remove(environest);
					remove(econest);
					remove(futuristest);
					remove(swayText);

					gameData.game.playerone.money -= (totalCost * 1000);

					for (i in 1...7) {
						if ((environ[i] != 0 || econ[i] != 0 || futurist[i] != 0) && campaign[i] != 0) {
							var endec = environ[i] / 100;
							var ecdec = econ[i] / 100;
							var futdec = futurist[i] / 100;
							var endiff;
							var ecdiff;
							var fudiff;
							var swayed:Int = 0;

							if (endec > envirplan[i]) {
								endiff = (endec - envirplan[i]) / endec;
							} else { 
								endiff = (envirplan[i] - endec) / envirplan[i]; 
							}

							if (ecdec > econplan[i]) {
								ecdiff = (ecdec - econplan[i]) / ecdec;
							} else {
								ecdiff = (econplan[i] - ecdec) / econplan[i];
							}

							if (futdec > futplan[i]) {
								fudiff = (futdec - futplan[i]) / futdec;
							} else {
								fudiff = (futplan[i] - futdec) / futplan[i];
							}

							var percentMod = (100 - (fudiff + ecdec + endec)) + (campaign[i] / 1000);

							for (i in 0...population[i] + 1) {
								if (Math.random() <= percentMod) {
									swayed += 1;
								}
							}

							gameData.game.playerone.money += (swayed * .1);

							if (i == 1) {
								gameData.game.planetone.neither -= swayed;
								gameData.game.planetone.userone += swayed;
							}
							else if (i == 2) {
								gameData.game.planettwo.neither -= swayed;
								gameData.game.planettwo.userone += swayed;
							}
							else if (i == 3) {
								gameData.game.planetthree.neither -= swayed;
								gameData.game.planetthree.userone += swayed;
							}
							else if (i == 4) {
								gameData.game.planetfour.neither -= swayed;
								gameData.game.planetfour.userone += swayed;
							}
							else if (i == 5) {
								gameData.game.planetfive.neither -= swayed;
								gameData.game.planetfive.userone += swayed;
							}
							else if (i == 6) {
								gameData.game.planetsix.neither -= swayed;
								gameData.game.planetsix.userone += swayed;
							}
						}

						if (research[i] != 0) {
							var researchmod = (100 - research[i]) / 100;
							var negamod = 1 - researchmod;
							var researchreturns = research[i] * .25;

							researchmod += randomRange(-negamod, negamod);

							if (i == 1) {
								percentData.percents.planetone.environ = gameData.game.planetone.environ * researchmod;
								percentData.percents.planetone.econ = gameData.game.planetone.econ * researchmod;
								percentData.percents.planetone.futurist = gameData.game.planetone.futurist * researchmod;
							}
							else if (i == 2) {
								percentData.percents.planettwo.environ = gameData.game.planettwo.environ * researchmod;
								percentData.percents.planettwo.econ = gameData.game.planettwo.econ * researchmod;
								percentData.percents.planettwo.futurist = gameData.game.planettwo.futurist * researchmod;
							}
							else if (i == 3) {
								percentData.percents.planetthree.environ = gameData.game.planetthree.environ * researchmod;
								percentData.percents.planetthree.econ = gameData.game.planetthree.econ * researchmod;
								percentData.percents.planetthree.futurist = gameData.game.planetthree.futurist * researchmod;
							}
							else if (i == 4) {
								percentData.percents.planetfour.environ = gameData.game.planetfour.environ * researchmod;
								percentData.percents.planetfour.econ = gameData.game.planetfour.econ * researchmod;
								percentData.percents.planetfour.futurist = gameData.game.planetfour.futurist * researchmod;
							}
							else if (i == 5) {
								percentData.percents.planetfive.environ = gameData.game.planetfive.environ * researchmod;
								percentData.percents.planetfive.econ = gameData.game.planetfive.econ * researchmod;
								percentData.percents.planetfive.futurist = gameData.game.planetfive.futurist * researchmod;
							}
							else if (i == 6) {
								percentData.percents.planetsix.environ = gameData.game.planetsix.environ * researchmod;
								percentData.percents.planetsix.econ = gameData.game.planetsix.econ * researchmod;
								percentData.percents.planetsix.futurist = gameData.game.planetsix.futurist * researchmod;
							}
							gameData.game.playerone.money += (researchreturns * 1000);
							/*var mod1;
							var mod2;
							var mod3;
							
							if (Math.random() > .5) {
								mod1 = researchmod + Math.random();
							} else {
								mod1 = researchmod - Math.random();
							}

							if (Math.random() > .5) {
								mod2 = researchmod + Math.random();
							} else {
								mod2 = researchmod - Math.random();
							}

							if (Math.random() > .5) {
								mod3 = researchmod + Math.random();
							} else {
								mod3 = researchmod - Math.random();
							}

							if (i == 1) {
								percentData.percents.planetone.environ = gameData.game.planetone.environ + mod1;
								percentData.percents.planetone.econ = gameData.game.planetone.econ + mod2;
								percentData.percents.planetone.futurist = gameData.game.planetone.futurist + mod3;
							}
							else if (i == 2) {
								percentData.percents.planettwo.environ = gameData.game.planettwo.environ + mod1;
								percentData.percents.planettwo.econ = gameData.game.planettwo.econ + mod2;
								percentData.percents.planettwo.futurist = gameData.game.planettwo.futurist + mod3;
							}
							else if (i == 3) {
								percentData.percents.planetthree.environ = gameData.game.planetthree.environ + mod1;
								percentData.percents.planetthree.econ = gameData.game.planetthree.econ + mod2;
								percentData.percents.planetthree.futurist = gameData.game.planetthree.futurist + mod3;
							}
							else if (i == 4) {
								percentData.percents.planetfour.environ = gameData.game.planetfour.environ + mod1;
								percentData.percents.planetfour.econ = gameData.game.planetfour.econ + mod2;
								percentData.percents.planetfour.futurist = gameData.game.planetfour.futurist + mod3;
							}
							else if (i == 5) {
								percentData.percents.planetfive.environ = gameData.game.planetfive.environ + mod1;
								percentData.percents.planetfive.econ = gameData.game.planetfive.econ + mod2;
								percentData.percents.planetfive.futurist = gameData.game.planetfive.futurist + mod3;
							}
							else if (i == 6) {
								percentData.percents.planetsix.environ = gameData.game.planetsix.environ + mod1;
								percentData.percents.planetsix.econ = gameData.game.planetsix.econ + mod2;
								percentData.percents.planetsix.futurist = gameData.game.planetsix.futurist + mod3;
							}*/
						}
					}

					var fname = fnm + ".sav";
					var fname2 = fnm + ".dat";

					var fout = cpp.io.File.write(fname, false);
					var sav = new GameJSON(gameData.game.turn, 2, gameData.game.playerone.password, gameData.game.playerone.portrait, gameData.game.playerone.money, gameData.game.playertwo.password, gameData.game.playertwo.portrait, gameData.game.playertwo.money, gameData.game.planetone.population, gameData.game.planetone.neither, gameData.game.planetone.userone, gameData.game.planetone.usertwo, gameData.game.planetone.environ, gameData.game.planetone.econ, gameData.game.planetone.futurist, gameData.game.planettwo.population, gameData.game.planettwo.neither, gameData.game.planettwo.userone, gameData.game.planettwo.usertwo, gameData.game.planettwo.environ, gameData.game.planettwo.econ, gameData.game.planettwo.futurist, gameData.game.planetthree.population, gameData.game.planetthree.neither, gameData.game.planetthree.userone, gameData.game.planetthree.usertwo, gameData.game.planetthree.environ, gameData.game.planetthree.econ, gameData.game.planetthree.futurist, gameData.game.planetfour.population, gameData.game.planetfour.neither, gameData.game.planetfour.userone, gameData.game.planetfour.usertwo, gameData.game.planetfour.environ, gameData.game.planetfour.econ, gameData.game.planetfour.futurist, gameData.game.planetfive.population, gameData.game.planetfive.neither, gameData.game.planetfive.userone, gameData.game.planetfive.usertwo, gameData.game.planetfive.environ, gameData.game.planetfive.econ, gameData.game.planetfive.futurist, gameData.game.planetsix.population, gameData.game.planetsix.neither, gameData.game.planetsix.userone, gameData.game.planetsix.usertwo, gameData.game.planetsix.environ, gameData.game.planetsix.econ, gameData.game.planetsix.futurist);
					fout.writeString(sav.jsonstring);
					fout.close();

					fout = cpp.io.File.write(fname2, false);
					var dat = new PercentJSON(percentData.percents.planetone.environ, percentData.percents.planetone.econ, percentData.percents.planetone.futurist, percentData.percents.planettwo.environ, percentData.percents.planettwo.econ, percentData.percents.planettwo.futurist, percentData.percents.planetthree.environ, percentData.percents.planetthree.econ, percentData.percents.planetthree.futurist, percentData.percents.planetfour.environ, percentData.percents.planetfour.econ, percentData.percents.planetfour.futurist, percentData.percents.planetfive.environ, percentData.percents.planetfive.econ, percentData.percents.planetfive.futurist, percentData.percents.planetsix.environ, percentData.percents.planetsix.econ, percentData.percents.planetsix.futurist);
					fout.writeString(dat.jsonstring);
					fout.close();

					HXP.scene = new MainMenu("Turn successfully submitted.");
				}
				else if (actv == 2) {
					remove(endTurn);
					remove(trn);
					remove(cashtext);
					remove(esttext);
					remove(environest);
					remove(econest);
					remove(futuristest);

					gameData.game.playertwo.money -= (totalCost * 1000);

					for (i in 1...7) {
						if ((environ[i] != 0 || econ[i] != 0 || futurist[i] != 0) && campaign[i] != 0) {
							var endec = environ[i] / 100;
							var ecdec = econ[i] / 100;
							var futdec = futurist[i] / 100;
							var endiff;
							var ecdiff;
							var fudiff;
							var swayed:Int = 0;

							if (endec > envirplan[i]) {
								endiff = (endec - envirplan[i]) / endec;
							} else { 
								endiff = (envirplan[i] - endec) / envirplan[i]; 
							}

							if (ecdec > econplan[i]) {
								ecdiff = (ecdec - econplan[i]) / ecdec;
							} else {
								ecdiff = (econplan[i] - ecdec) / econplan[i];
							}

							if (futdec > futplan[i]) {
								fudiff = (futdec - futplan[i]) / futdec;
							} else {
								fudiff = (futplan[i] - futdec) / futplan[i];
							}

							var percentMod = (100 - (fudiff + ecdec + endec)) + (campaign[i] / 1000);

							for (i in 0...population[i] + 1) {
								if (Math.random() <= percentMod) {
									swayed += 1;
								}
							}

							gameData.game.playertwo.money += (swayed * .1);

							if (i == 1) {
								gameData.game.planetone.neither -= swayed;
								gameData.game.planetone.usertwo += swayed;
							}
							else if (i == 2) {
								gameData.game.planettwo.neither -= swayed;
								gameData.game.planettwo.usertwo += swayed;
							}
							else if (i == 3) {
								gameData.game.planetthree.neither -= swayed;
								gameData.game.planetthree.usertwo += swayed;
							}
							else if (i == 4) {
								gameData.game.planetfour.neither -= swayed;
								gameData.game.planetfour.usertwo += swayed;
							}
							else if (i == 5) {
								gameData.game.planetfive.neither -= swayed;
								gameData.game.planetfive.usertwo += swayed;
							}
							else if (i == 6) {
								gameData.game.planetsix.neither -= swayed;
								gameData.game.planetsix.usertwo += swayed;
							}
						}

						if (research[i] != 0) {
							var researchmod = (100 - research[i]) / 100;
							var negamod = 1 - researchmod;
							var researchreturns = research[i] * .25;

							researchmod += randomRange(-negamod, negamod);
							/*if (Math.random() > .5) {
								researchmod = researchmod + (researchmod * Math.random());
							} else {
								researchmod = researchmod - (researchmod * Math.random());
							}*/

							if (i == 1) {
								percentData.percents.planetone.environ = gameData.game.planetone.environ * researchmod;
								percentData.percents.planetone.econ = gameData.game.planetone.econ * researchmod;
								percentData.percents.planetone.futurist = gameData.game.planetone.futurist * researchmod;
							}
							else if (i == 2) {
								percentData.percents.planettwo.environ = gameData.game.planettwo.environ * researchmod;
								percentData.percents.planettwo.econ = gameData.game.planettwo.econ * researchmod;
								percentData.percents.planettwo.futurist = gameData.game.planettwo.futurist * researchmod;
							}
							else if (i == 3) {
								percentData.percents.planetthree.environ = gameData.game.planetthree.environ * researchmod;
								percentData.percents.planetthree.econ = gameData.game.planetthree.econ * researchmod;
								percentData.percents.planetthree.futurist = gameData.game.planetthree.futurist * researchmod;
							}
							else if (i == 4) {
								percentData.percents.planetfour.environ = gameData.game.planetfour.environ * researchmod;
								percentData.percents.planetfour.econ = gameData.game.planetfour.econ * researchmod;
								percentData.percents.planetfour.futurist = gameData.game.planetfour.futurist * researchmod;
							}
							else if (i == 5) {
								percentData.percents.planetfive.environ = gameData.game.planetfive.environ * researchmod;
								percentData.percents.planetfive.econ = gameData.game.planetfive.econ * researchmod;
								percentData.percents.planetfive.futurist = gameData.game.planetfive.futurist * researchmod;
							}
							else if (i == 6) {
								percentData.percents.planetsix.environ = gameData.game.planetsix.environ * researchmod;
								percentData.percents.planetsix.econ = gameData.game.planetsix.econ * researchmod;
								percentData.percents.planetsix.futurist = gameData.game.planetsix.futurist * researchmod;
							}
							gameData.game.playertwo.money += (researchreturns * 1000);
						}
					}

					var fname = fnm + ".sav";
					var fname2 = fnm + ".dat";

					var fout = cpp.io.File.write(fname, false);
					var sav = new GameJSON(gameData.game.turn + 1, 1, gameData.game.playerone.password, gameData.game.playerone.portrait, gameData.game.playerone.money, gameData.game.playertwo.password, gameData.game.playertwo.portrait, gameData.game.playertwo.money, gameData.game.planetone.population, gameData.game.planetone.neither, gameData.game.planetone.userone, gameData.game.planetone.usertwo, gameData.game.planetone.environ, gameData.game.planetone.econ, gameData.game.planetone.futurist, gameData.game.planettwo.population, gameData.game.planettwo.neither, gameData.game.planettwo.userone, gameData.game.planettwo.usertwo, gameData.game.planettwo.environ, gameData.game.planettwo.econ, gameData.game.planettwo.futurist, gameData.game.planetthree.population, gameData.game.planetthree.neither, gameData.game.planetthree.userone, gameData.game.planetthree.usertwo, gameData.game.planetthree.environ, gameData.game.planetthree.econ, gameData.game.planetthree.futurist, gameData.game.planetfour.population, gameData.game.planetfour.neither, gameData.game.planetfour.userone, gameData.game.planetfour.usertwo, gameData.game.planetfour.environ, gameData.game.planetfour.econ, gameData.game.planetfour.futurist, gameData.game.planetfive.population, gameData.game.planetfive.neither, gameData.game.planetfive.userone, gameData.game.planetfive.usertwo, gameData.game.planetfive.environ, gameData.game.planetfive.econ, gameData.game.planetfive.futurist, gameData.game.planetsix.population, gameData.game.planetsix.neither, gameData.game.planetsix.userone, gameData.game.planetsix.usertwo, gameData.game.planetsix.environ, gameData.game.planetsix.econ, gameData.game.planetsix.futurist);
					fout.writeString(sav.jsonstring);
					fout.close();

					fout = cpp.io.File.write(fname2, false);
					var dat = new PercentJSON(percentData.percents.planetone.environ, percentData.percents.planetone.econ, percentData.percents.planetone.futurist, percentData.percents.planettwo.environ, percentData.percents.planettwo.econ, percentData.percents.planettwo.futurist, percentData.percents.planetthree.environ, percentData.percents.planetthree.econ, percentData.percents.planetthree.futurist, percentData.percents.planetfour.environ, percentData.percents.planetfour.econ, percentData.percents.planetfour.futurist, percentData.percents.planetfive.environ, percentData.percents.planetfive.econ, percentData.percents.planetfive.futurist, percentData.percents.planetsix.environ, percentData.percents.planetsix.econ, percentData.percents.planetsix.futurist);
					fout.writeString(dat.jsonstring);
					fout.close();

					HXP.scene = new MainMenu("Turn successfully submitted.");
				}
			}
		} else {
			button.color = 0x393939;
		}

		super.update();
	}

	private function printSelection(e:Dynamic)
	{
		trace(car.selectedId);
	}

	private function leftSelect(e:Dynamic)
	{
		var i = car.selectedId;

		if (selected[i] == 0) {
			if (research[i] >= 10) {
				research[i] -= 10;
				lbls[i].text = "$" + research[i] + "k";
				totalCost -= 10;
			}
		} else if (selected[i] == 1) {
			if (campaign[i] >= 10) {
				campaign[i] -= 10;
				lbls[i].text = "$" + campaign[i] + "k";
				totalCost -= 10;
			}
		} else if (selected[i] == 2) {
			if (environ[i] >= 2) {
				environ[i] -= 2;
				lbls[i].text = environ[i] + "%";
			}
		} else if (selected[i] == 3) {
			if (econ[i] >= 2) {
				econ[i] -= 2;
				lbls[i].text = econ[i] + "%";
			}
		} else if (selected[i] == 4) {
			if (futurist[i] >= 2) {
				futurist[i] -= 2;
				lbls[i].text = futurist[i] + "%";
			}
		}
	}

	private function centerSelect(e:Dynamic)
	{
		var i = car.selectedId;

		if (selected[i] == 0) {
			selected[i] = 1;
			lbls[i].text = "$" + campaign[i] + "k";
			textBt[i].text = "Campaign";
		} else if (selected[i] == 1) {
			selected[i] = 2;
			lbls[i].text = environ[i] + "%";
			textBt[i].text = "Environ";
		} else if (selected[i] == 2) {
			selected[i] = 3;
			lbls[i].text = econ[i] + "%";
			textBt[i].text = "Economist";
		} else if (selected[i] == 3) {
			selected[i] = 4;
			lbls[i].text = futurist[i] + "%";
			textBt[i].text = "Futurist";
		} else if (selected[i] == 4) {
			selected[i] = 0;
			lbls[i].text = "$" + research[i] + "k";
			textBt[i].text = "Research";
		}
	}

	private function rightSelect(e:Dynamic)
	{
		var i = car.selectedId;

		if (selected[i] == 0) {
			if (research[i] <= 90 && (totalCost + 10) < money) {
				research[i] += 10;
				lbls[i].text = "$" + research[i] + "k";
				totalCost += 10;
			}
		} else if (selected[i] == 1 && (totalCost + 10) < money)  {
			if (campaign[i] <= 90) {
				campaign[i] += 10;
				lbls[i].text = "$" + campaign[i] + "k";
				totalCost += 10;
			}
		} else if (selected[i] == 2) {
			if (environ[i] <= 98) {
				environ[i] += 2;
				lbls[i].text = environ[i] + "%";
			}
		} else if (selected[i] == 3) {
			if (econ[i] <= 98) {
				econ[i] += 2;
				lbls[i].text = econ[i] + "%";
			}
		} else if (selected[i] == 4) {
			if (futurist[i] <= 98) {
				futurist[i] += 2;
				lbls[i].text = futurist[i] + "%";
			}
		}
	}

	private function arrayUpdate()
	{
		for (i in 1...7) {
			if (research[i] > 0) {
				campaign[i] = 0;
				environ[i] = 0;
				econ[i] = 0;
				futurist[i] = 0;
			}
		}

		environest.menuText.text = "Environ: " + Std.string(envir[car.selectedId]);
		econest.menuText.text = "Econ: " + Std.string(econe[car.selectedId]);
		futuristest.menuText.text = "Futurist: " + Std.string(fute[car.selectedId]);
		swayText.menuText.text = "Swayed: " + Std.string(sway[car.selectedId]);
	}

	private function randomRange(max:Float, min:Float = 0)
	{
		return Math.random() * (max - min) + min;
	}
}