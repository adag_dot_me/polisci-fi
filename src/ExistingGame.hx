import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Scene;
import com.haxepunk.graphics.Image;
import com.haxepunk.utils.Input;
import menu.MenuText;
import menu.TextInput;
import menu.RotatingBackground;
import menu.MenuPortrait;
import sys.io.File;
import sys.FileSystem;
import game.GameJSON;
import nme.filesystem.File;
import tjson.TJSON;

class ExistingGame extends Scene
{
	private var button:Image;
	private var box:Image;
	private var box2:Image;
	private var name:MenuText;
	private var pass:MenuText;
	private var nameInput:TextInput;
	private var passInput:TextInput;
	private var loadGame:MenuText;
	private var title:MenuText;
	private var sav:String;
	private var bg:RotatingBackground;
	private var prt:MenuPortrait;

	public function new()
	{
		super();
	}

	public override function begin()
	{
		title = new MenuText("PoliSci-Fi", 290, 40, 40, "font/AmpedExpItalic.otf");
		add(title);

		name = new MenuText("Enter game name:", 100, 215, 20);
		add(name);

		bg = new RotatingBackground(600, 400);
		add(bg);

		prt = new MenuPortrait(506, 335, 1);
		add(prt);

		box = new Image("gfx/button.png");
		box.color = 0x393939;
		box.x = 300;
		box.y = 200;
		addGraphic(box);

		nameInput = new TextInput(307, 215, box);
		add(nameInput);

		pass = new MenuText("Enter password:", 100, 315, 20);
		add(pass);

		box2 = new Image("gfx/button.png");
		box2.color = 0x393939;
		box2.x = 300;
		box2.y = 300;
		addGraphic(box2);

		passInput = new TextInput(307, 315, box2);
		add(passInput);

		button = new Image("gfx/button.png");
		button.color = 0x393939;
		button.x = 300;
		button.y = 400;
		addGraphic(button);

		loadGame = new MenuText("Load Game", 320, 410, 20);
		add(loadGame);
	}

	public override function update()
	{
		if (Input.mouseX > 300 && Input.mouseX < 500 && Input.mouseY > 400 && Input.mouseY < 450) {
			button.color = 0x525252;
			if (Input.mousePressed) {
				remove(name);
				remove(pass);
				remove(title);
				remove(loadGame);

				var fnam = nameInput.text;
				var fname = Std.string(nameInput.text) + ".sav";
				var pass = Std.string(passInput.text);
				remove(nameInput);
				remove(passInput);

				if (sys.FileSystem.exists(fname) == false) {
					HXP.scene = new MainMenu("File " + fname + " does not exist.");
				}
				if (sys.FileSystem.exists(fname) == true) {
					var fin = cpp.io.File.read(fname, false);
					trace("fin initialized");
					var fintext = fin.readLine();
					trace(fintext);
					var gameData = TJSON.parse(fintext);
					trace(gameData.game.playerone.password);

					var totalPop = gameData.game.planetone.neither + gameData.game.planettwo.neither + gameData.game.planetthree.neither + gameData.game.planetfour.neither + gameData.game.planetfive.neither + gameData.game.planetsix.neither;

					if (totalPop == 0) {
						var totalOne = gameData.game.planetone.playerone + gameData.game.planettwo.playerone + gameData.game.planetthree.playerone + gameData.game.planetfour.playerone + gameData.game.planetfive.playerone + gameData.game.planetsix.playerone;
						var totalTwo = gameData.game.planetone.playertwo + gameData.game.planettwo.playertwo + gameData.game.planetthree.playertwo + gameData.game.planetfour.playertwo + gameData.game.planetfive.playertwo + gameData.game.planetsix.playertwo;
						
						if (totalOne > totalTwo) {
							HXP.scene = new MainMenu("Player one wins.");
						} else if (totalTwo > totalOne) {
							HXP.scene = new MainMenu("Player two wins.");
						}
					}

					if (gameData.game.playertwo.password == "none") {
						HXP.scene = new EnterPassword(fnam);
					}
					else if (gameData.game.playertwo.password == pass) {
						if (gameData.game.active == 2) {
							HXP.scene = new GameScene(2, fnam);
						}
						else {
							HXP.scene = new MainMenu("It is not currently your turn.");
						}

					}
					else if (gameData.game.playerone.password == pass) {
						if (gameData.game.active == 1) {
							HXP.scene = new GameScene(1, fnam);
						}
						else {
							HXP.scene = new MainMenu("It is not currently your turn.");
						}
					}
					else {
						HXP.scene = new MainMenu("Your password is incorrect.");
					}
				}
		} else {
			button.color = 0x393939;
		}
		}
		//checkMouse();
		//checkClicked();
		super.update();
	}
	
}