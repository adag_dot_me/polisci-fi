package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;

class RotatingBackground extends Entity
{
	private var sprite:Spritemap;

	public function new(x:Float, y:Float)
	{
		super(x, y);

		sprite = new Spritemap("gfx/backgroundstars.png", 1755, 1504);
		sprite.centerOrigin();
		graphic = sprite;
		layer = 400;
	}

	override public function update() {
		sprite.angle += 0.2;
		super.update();
	}
}