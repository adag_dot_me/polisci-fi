package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.Sfx;

class MenuPortrait extends Entity
{
	private var sprite:Spritemap;

	public function new(x:Float, y:Float, p:Int, ?l:Int)
	{
		super(x, y);

		if (p == 0) {
			sprite = new Spritemap("gfx/portrait1.png", 274, 272);
		}
		else if (p == 1) {
			sprite = new Spritemap("gfx/portrait2.png", 274, 255);
		}
		else if (p == 2) {
			sprite = new Spritemap("gfx/portrait3.png", 251, 272);
		}

		graphic = sprite;

		if (l != null) {
			layer = l;
		} else {
			layer = 1;
		}
	}

	override public function update() {
		super.update();
	}
}