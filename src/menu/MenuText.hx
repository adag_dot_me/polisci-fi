package menu;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Draw;

class MenuText extends Entity 
{
	public var menuText:Text;

	public function new(_text:String, _x:Float, _y:Float, ?_size:Int, ?_font:String, ?center:Bool)
	{
		super(_x, _y);
		//trace("super");

		menuText = new Text(_text);

		if (_size != null) {
			menuText.size = _size;
		} else {
			menuText.size = 10;
		}

		if (_font != null) {
			menuText.font = _font;
		}

		if (center == true) {
			menuText.centerOrigin();
		}

		graphic = menuText;
		x = _x;
		y = _y;
		layer = -10;
	}

	override public function update()
	{
		super.update();
	}
}