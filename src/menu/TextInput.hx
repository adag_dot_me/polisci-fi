package menu;

import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Draw;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import nme.events.KeyboardEvent;

class TextInput extends Entity 
{
	private var _text:String;
	public var text:String;
	private var textGraphic:Text;
	private var bg:Image;

	private var focus:Bool = false;

	public function new(_x:Float, _y:Float, _bg:Image)
	{
		super(_x, _y);
		//trace("super");

		text = "";
		textGraphic = new Text(text, 0, 0, { size : 20 });
		textGraphic.color = 0x525252;

		graphic = textGraphic;
		x = _x;
		y = _y;
		bg = _bg;
		layer = -10;

		type = "textInput";

		setHitbox(190, 40);
	}

	override public function added()
	{
		super.update();

		HXP.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
	}

	private function onKeyDown(e:KeyboardEvent)
	{
		if (world != HXP.world) { 
			return; 
		}

		if (focus != true) { 
			return; 
		}

		if (e.keyCode == Key.BACKSPACE) {
			text = text.substr(0, text.length - 1);
		}
	}

	override public function update()
	{
		if (focus == true) {
			bg.color = 0x525252;
			textGraphic.color = 0xFFFFFF;

			if (Input.keyString != "")
			{
				text += Input.keyString;
				Input.keyString = "";
			}
		} else {
			bg.color = 0x393939;
			textGraphic.color = 0x525252;
		}

		if (Input.mousePressed)
		{
			if (collidePoint(x, y, world.mouseX, world.mouseY))
			{
				focus = true;
				Input.keyString = "";
			}
			else if (world.collidePoint("uiTextInput", world.mouseX, world.mouseY) != this) {
				focus = false;
			}
		}

		if (text.length > 14) {
			text = text.substr(0, text.length - 1);
		}

		textGraphic.text = text;

		super.update();
	}
}