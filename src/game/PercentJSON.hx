package game;

class PercentJSON
{
	public var jsonstring:String;

	public function new(pl1environ:Float, pl1econ:Float, pl1fut:Float, pl2environ:Float, pl2econ:Float, pl2fut:Float, pl3environ:Float, pl3econ:Float, pl3fut:Float, pl4environ:Float, pl4econ:Float, pl4fut:Float, pl5environ:Float, pl5econ:Float, pl5fut:Float, pl6environ:Float, pl6econ:Float, pl6fut:Float)
	{
		jsonstring = "{ 'percents' : { 'planetone' : { 'environ' : " + pl1environ + ", 'econ' : " + pl1econ + ", 'futurist' : " + pl1fut + " }, 'planettwo' : { 'environ' : " + pl2environ + ", 'econ' : " + pl2econ + ", 'futurist' : " + pl2fut + " }, 'planetthree' : { 'environ' : " + pl3environ + ", 'econ' : " + pl3econ + ", 'futurist' : " + pl3fut + " }, 'planetfour' : { 'environ' : " + pl4environ + ", 'econ' : " + pl4econ + ", 'futurist' : " + pl4fut + " }, 'planetfive' : { 'environ' : " + pl5environ + ", 'econ' : " + pl5econ + ", 'futurist' : " + pl5fut + " }, 'planetsix' : { 'environ' : " + pl6environ + ", 'econ' : " + pl6econ + ", 'futurist' : " + pl6fut + " } } }";
	}
}